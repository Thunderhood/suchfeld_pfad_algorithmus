# Todoliste:

## Datenbank
* [x] Zahlen werden in Datenbank abgespeichert.
* [x] zukünftige Zahlen werden in eine seperate Tabelle gespeichert.



## Controller
* [x] **GET** alle Zahlen, die im Pfad der gesuchten Zahl sind.
  * notwendig: Rho, Zahl 
* [x] **GET** alle Zahlen, in der Haupttabelle.
  *   notwendig: Rho
* [ ] **GET** alle Zahlen, in der Haupttabelle (unabhängig vom Rho-Wert).
* [x] **DELETE** lösche alle Zahlen die einen den angegebenen Rho-Wert haben.
  * alle zukünftigen Zahlen, mit dem jeweiligen Rho-Wert, werden auch gelöscht. 
* [ ] Multithreading um mehrere Anfragen zu bearbeiten

## Service
* [ ] Die Menge der nächsten Zahlen, dessen Nachfolger berechnet werden soll, in etwa N gleich große Listen aufspalten und jede von einem Thread bearbeiten zu lassen.
* [ ] .

## Frontend

