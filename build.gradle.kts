import java.time.LocalDateTime

plugins {
    id("org.springframework.boot") version "2.6.7"
    id("io.spring.dependency-management") version "1.0.11.RELEASE"
    id("java")
}

group = "kleiner.suchpfad"
version = "1.0-SNAPSHOT"


repositories {
    mavenCentral()
}

dependencies {
    testImplementation("org.junit.jupiter:junit-jupiter-api:5.8.2")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:5.8.2")
    implementation ("org.springframework.boot:spring-boot-starter-web")
    implementation ("org.springframework.boot:spring-boot-starter")
    testImplementation ("org.springframework.boot:spring-boot-starter-test")
    implementation ("org.springframework.boot:spring-boot-starter-data-jpa")
    implementation ("mysql:mysql-connector-java:8.0.29")
    implementation("org.flywaydb:flyway-core")
}

tasks.bootJar()
{
    from("spa-frontend/dist/spa-frontend")
    {
        include("*")
        into("public")
    }
}

tasks.processResources()
{
    dependsOn("spa-frontend:build")
}


tasks.getByName<Test>("test") {
    useJUnitPlatform()
}


tasks.jar {
    archiveFileName.set("SPA.jar")
    manifest {
        attributes(
            "Build-Timestamp" to LocalDateTime.now().toString(),
            "Main-Class" to "kleiner.suchpfad.Start",
        )
    }
}




