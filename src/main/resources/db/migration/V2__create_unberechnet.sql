CREATE TABLE unberechnet (
  id BIGINT AUTO_INCREMENT NOT NULL,
   rechenoperation INT NULL,
   orginal_id BIGINT NULL,
   rho BIGINT NULL,
   wert BIGINT NULL,
   zeilenindex BIGINT NULL,
   spaltenindex BIGINT NULL,
   vorgaenger_id BIGINT NULL,
   rechte_zahl_id BIGINT NULL,
   untere_zahl_id BIGINT NULL,
   linke_zahl_id BIGINT NULL,
   obere_zahl_id BIGINT NULL,
   CONSTRAINT pk_unberechnet PRIMARY KEY (id)
);