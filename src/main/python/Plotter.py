import matplotlib.pyplot as plt

# Read Dataset

target1 = []
number_of_steps1 = []

data1 = open('../python/Datenset/Datenset_rho_0(2-1000)', 'r')
for line in data1:
    splitted = line.split(' ')
    target1.append(splitted[0])
    number_of_steps1.append(len(splitted))

target2 = []
number_of_steps2 = []

data2 = open('../python/Datenset/Datenset_rho_3(2-1000)', 'r')
for line in data2:
    splitted = line.split(' ')
    target2.append(splitted[0])
    number_of_steps2.append(len(splitted))

target3 = []
number_of_steps3 = []

data3 = open('../python/Datenset/Datenset_rho_7(2-1000)', 'r')
for line in data3:
    splitted = line.split(' ')
    target3.append(splitted[0])
    number_of_steps3.append(len(splitted))

target4 = []
number_of_steps4 = []

data4 = open('../python/Datenset/Datenset_rho_9(2-1000)', 'r')
for line in data4:
    splitted = line.split(' ')
    target4.append(splitted[0])
    number_of_steps4.append(len(splitted))

target5 = []
number_of_steps5 = []

data5 = open('../python/Datenset/Datenset_rho_11(2-1000)', 'r')
for line in data5:
    splitted = line.split(' ')
    target5.append(splitted[0])
    number_of_steps5.append(len(splitted))

target6 = []
number_of_steps6 = []

data6 = open('../python/Datenset/Datenset_rho_14(2-1000)', 'r')
for line in data6:
    splitted = line.split(' ')
    target6.append(splitted[0])
    number_of_steps6.append(len(splitted))

target7 = []
number_of_steps7 = []

data7 = open('../python/Datenset/Datenset_rho_16(2-1000)', 'r')
for line in data7:
    splitted = line.split(' ')
    target7.append(splitted[0])
    number_of_steps7.append(len(splitted))

target8 = []
number_of_steps8 = []

data8 = open('../python/Datenset/Datenset_rho_18(2-1000)', 'r')
for line in data8:
    splitted = line.split(' ')
    target8.append(splitted[0])
    number_of_steps8.append(len(splitted))

target9 = []
number_of_steps9 = []

data9 = open('../python/Datenset/Datenset_rho_21(2-1000)', 'r')
for line in data9:
    splitted = line.split(' ')
    target9.append(splitted[0])
    number_of_steps9.append(len(splitted))

target10 = []
number_of_steps10 = []

data10 = open('../python/Datenset/Datenset_rho_24(2-1000)', 'r')
for line in data10:
    splitted = line.split(' ')
    target10.append(splitted[0])
    number_of_steps10.append(len(splitted))

target11 = []
number_of_steps11 = []

data11 = open('../python/Datenset/Datenset_rho_167(2-1000)', 'r')
for line in data11:
    splitted = line.split(' ')
    target11.append(splitted[0])
    number_of_steps11.append(len(splitted))

target12 = []
number_of_steps12 = []

data12 = open('../python/Datenset/Datenset_rho_302(2-1000)', 'r')
for line in data12:
    splitted = line.split(' ')
    target12.append(splitted[0])
    number_of_steps12.append(len(splitted))

# die X-Werte:


# die Y-Werte:
plt.xlabel('Target')
plt.ylabel('Number of Steps')
plt.xticks([0, 100, 200, 300, 400, 500, 600, 700, 800, 900, 1000])
plt.plot(target1, number_of_steps1, label='rho_0(2-1000)')
plt.plot(target2, number_of_steps2, label='rho_3(2-1000)')
plt.plot(target3, number_of_steps3, label='rho_7(2-1000)')
plt.plot(target4, number_of_steps4, label='rho_9(2-1000)')
plt.plot(target5, number_of_steps5, label='rho_11(2-1000)')
plt.plot(target6, number_of_steps6, label='rho_14(2-1000)')
plt.plot(target7, number_of_steps7, label='rho_16(2-1000)')
plt.plot(target8, number_of_steps8, label='rho_18(2-1000)')
plt.plot(target9, number_of_steps9, label='rho_21(2-1000)')
plt.plot(target10, number_of_steps10, label='rho_24(2-1000)')
plt.plot(target11, number_of_steps11, label='rho_167(2-1000)')
plt.plot(target12, number_of_steps12, label='rho_302(2-1000)')
plt.legend(loc='upper left')
plt.grid(True)
plt.savefig('Plots/rho_0-302(2-1000).png', bbox_inches='tight')
plt.clf()

# die Y-Werte:
plt.xlabel('Target')
plt.ylabel('Number of Steps')
plt.xticks([0, 100, 200, 300, 400, 500, 600, 700, 800, 900, 1000])
plt.plot(target1, number_of_steps1, label='rho_0(2-1000)')
plt.legend(loc='upper left')
plt.grid(True)
plt.savefig('Plots/rho_0(2-1000).png', bbox_inches='tight')
plt.clf()

plt.xlabel('Target')
plt.ylabel('Number of Steps')
plt.xticks([0, 100, 200, 300, 400, 500, 600, 700, 800, 900, 1000])
plt.plot(target2, number_of_steps2, label='rho_3(2-1000)')
plt.legend(loc='upper left')
plt.grid(True)
plt.savefig('Plots/rho_3(2-1000).png', bbox_inches='tight')
plt.clf()

plt.xlabel('Target')
plt.ylabel('Number of Steps')
plt.xticks([0, 100, 200, 300, 400, 500, 600, 700, 800, 900, 1000])
plt.plot(target3, number_of_steps3, label='rho_7(2-1000)')
plt.legend(loc='upper left')
plt.grid(True)
plt.savefig('Plots/rho_7(2-1000).png', bbox_inches='tight')
plt.clf()

plt.xlabel('Target')
plt.ylabel('Number of Steps')
plt.xticks([0, 100, 200, 300, 400, 500, 600, 700, 800, 900, 1000])
plt.plot(target4, number_of_steps4, label='rho_9(2-1000)')
plt.legend(loc='upper left')
plt.grid(True)
plt.savefig('Plots/rho_9(2-1000).png', bbox_inches='tight')
plt.clf()

plt.xlabel('Target')
plt.ylabel('Number of Steps')
plt.xticks([0, 100, 200, 300, 400, 500, 600, 700, 800, 900, 1000])
plt.plot(target5, number_of_steps5, label='rho_11(2-1000)')
plt.legend(loc='upper left')
plt.grid(True)
plt.savefig('Plots/rho_11(2-1000).png', bbox_inches='tight')
plt.clf()

plt.xlabel('Target')
plt.ylabel('Number of Steps')
plt.xticks([0, 100, 200, 300, 400, 500, 600, 700, 800, 900, 1000])
plt.plot(target6, number_of_steps6, label='rho_14(2-1000)')
plt.legend(loc='upper left')
plt.grid(True)
plt.savefig('Plots/rho_14(2-1000).png', bbox_inches='tight')
plt.clf()

plt.xlabel('Target')
plt.ylabel('Number of Steps')
plt.xticks([0, 100, 200, 300, 400, 500, 600, 700, 800, 900, 1000])
plt.plot(target7, number_of_steps7, label='rho_16(2-1000)')
plt.legend(loc='upper left')
plt.grid(True)
plt.savefig('Plots/rho_16(2-1000).png', bbox_inches='tight')
plt.clf()

plt.xlabel('Target')
plt.ylabel('Number of Steps')
plt.xticks([0, 100, 200, 300, 400, 500, 600, 700, 800, 900, 1000])
plt.plot(target8, number_of_steps8, label='rho_18(2-1000)')
plt.legend(loc='upper left')
plt.grid(True)
plt.savefig('Plots/rho_18(2-1000).png', bbox_inches='tight')
plt.clf()

plt.xlabel('Target')
plt.ylabel('Number of Steps')
plt.xticks([0, 100, 200, 300, 400, 500, 600, 700, 800, 900, 1000])
plt.plot(target9, number_of_steps9, label='rho_21(2-1000)')
plt.legend(loc='upper left')
plt.grid(True)
plt.savefig('Plots/rho_21(2-1000).png', bbox_inches='tight')
plt.clf()

plt.xlabel('Target')
plt.ylabel('Number of Steps')
plt.xticks([0, 100, 200, 300, 400, 500, 600, 700, 800, 900, 1000])
plt.plot(target10, number_of_steps10, label='rho_24(2-1000)')
plt.legend(loc='upper left')
plt.grid(True)
plt.savefig('Plots/rho_24(2-1000).png', bbox_inches='tight')
plt.clf()

plt.xlabel('Target')
plt.ylabel('Number of Steps')
plt.xticks([0, 100, 200, 300, 400, 500, 600, 700, 800, 900, 1000])
plt.plot(target11, number_of_steps11, label='rho_167(2-1000)')
plt.legend(loc='upper left')
plt.grid(True)
plt.savefig('Plots/rho_167(2-1000).png', bbox_inches='tight')
plt.clf()

plt.xlabel('Target')
plt.ylabel('Number of Steps')
plt.xticks([0, 100, 200, 300, 400, 500, 600, 700, 800, 900, 1000])
plt.plot(target12, number_of_steps12, label='rho_302(2-1000)')
plt.legend(loc='upper left')
plt.grid(True)
plt.savefig('Plots/rho_302(2-1000).png', bbox_inches='tight')
