package de.kleiner.suchpfad.model;

import java.util.List;


public class PfadEntity
{


    private String gesuchte_zahl;
    private String pfad_laenge = "";
    private String benoetigtessuchfeld = "";
    private String pfad_werte = "";
    private String pfad_coordinaten = "";

    private String erzeugende_formel = "";


    public PfadEntity()
    {
    }

    public PfadEntity(List<ZahlEntity> pfad_zur_Zahl, String zahl)
    {
        long max_zeilen_index = 0;
        long max_spalten_index = 0;

        this.pfad_laenge = String.valueOf(pfad_zur_Zahl.size() - 1);
        StringBuilder stringBuilder_Wert = new StringBuilder(pfad_werte);
        StringBuilder stringBuilder_Coordinate = new StringBuilder(pfad_werte);
        this.gesuchte_zahl = zahl;

        long akku = 0L;
        Long[] aktuelle_pos = new Long[2];
        Long[] betrachtete_pos = new Long[2];


        for (int index_zahl = 0; index_zahl < pfad_zur_Zahl.size(); index_zahl++)
        {
            stringBuilder_Coordinate.append("(").append(pfad_zur_Zahl.get(index_zahl).zeilenindex).append(", ").append(pfad_zur_Zahl.get(index_zahl).spaltenindex).append(")");

            //Ermitteln der benoetigten Suchfeldgroeße
            if (pfad_zur_Zahl.get(index_zahl).zeilenindex > max_zeilen_index)
            {
                max_zeilen_index = pfad_zur_Zahl.get(index_zahl).zeilenindex;
            }

            if (pfad_zur_Zahl.get(index_zahl).spaltenindex > max_spalten_index)
            {
                max_spalten_index = pfad_zur_Zahl.get(index_zahl).spaltenindex;
            }

            //Ermitteln der erzeugenden Formel

            if (index_zahl == 0)
            {
                aktuelle_pos[0] = pfad_zur_Zahl.get(index_zahl).zeilenindex;
                aktuelle_pos[1] = pfad_zur_Zahl.get(index_zahl).spaltenindex;
                this.erzeugende_formel="1";
            }
            else
            {
                betrachtete_pos[0] = pfad_zur_Zahl.get(index_zahl).zeilenindex;
                betrachtete_pos[1] = pfad_zur_Zahl.get(index_zahl).spaltenindex;

                if (betrachtete_pos[1] > aktuelle_pos[1])
                {
                    akku++;
                }
                else if (betrachtete_pos[1] < aktuelle_pos[1])
                {
                    akku--;
                }
                else if (betrachtete_pos[0] > aktuelle_pos[0])
                {
                    if (akku > 0)
                    {
                        this.erzeugende_formel = this.erzeugende_formel + "+" + akku;
                        akku = 0L;
                    }
                    else if (akku < 0)
                    {
                        this.erzeugende_formel = this.erzeugende_formel + akku;
                        akku = 0L;
                    }
                    this.erzeugende_formel = "(" + this.erzeugende_formel + ")" + "*" + betrachtete_pos[0];
                }
                else if (betrachtete_pos[0] < aktuelle_pos[0])
                {
                    if (akku > 0)
                    {
                        this.erzeugende_formel = this.erzeugende_formel + "+" + akku;
                        akku = 0L;
                    }
                    else if (akku < 0)
                    {
                        this.erzeugende_formel = this.erzeugende_formel + akku;
                        akku = 0L;
                    }
                    this.erzeugende_formel = "(" + this.erzeugende_formel + ")" + "/" + aktuelle_pos[0];
                }
                aktuelle_pos[0] = betrachtete_pos[0];
                aktuelle_pos[1] = betrachtete_pos[1];
            }
            //Ende der Formelerzeugung


            stringBuilder_Wert.append(pfad_zur_Zahl.get(index_zahl).getWert());
            if (index_zahl < pfad_zur_Zahl.size() - 1)
            {
                stringBuilder_Wert.append(" -> ");
                stringBuilder_Coordinate.append(" -> ");
            }
        }

        //Finalisieren der Formel
        if (akku > 0)
        {
            this.erzeugende_formel = this.erzeugende_formel + "+" + akku;
        }
        else if (akku < 0)
        {
            this.erzeugende_formel = this.erzeugende_formel + akku;
        }


        this.erzeugende_formel = "(" + this.erzeugende_formel + ")";
        this.pfad_werte = stringBuilder_Wert.toString();
        this.pfad_coordinaten = stringBuilder_Coordinate.toString();
        this.benoetigtessuchfeld = max_zeilen_index + "x" + max_spalten_index;

    }

    public String getGesuchte_zahl()
    {
        return gesuchte_zahl;
    }

    public String getPfad_laenge()
    {
        return pfad_laenge;
    }

    public String getBenoetigtessuchfeld()
    {
        return benoetigtessuchfeld;
    }

    public String getPfad_werte()
    {
        return pfad_werte;
    }

    public String getPfad_coordinaten()
    {
        return pfad_coordinaten;
    }

    public String getErzeugende_formel()
    {
        return erzeugende_formel;
    }
}
