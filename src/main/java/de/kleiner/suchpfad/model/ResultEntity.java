package de.kleiner.suchpfad.model;

import org.springframework.http.HttpStatus;

import java.util.List;


public class ResultEntity
{
    private List<PfadEntity> pfad;
    private List<String> fehlernachricht;

    private String bildpfad;

    private HttpStatus statusCode;

    public ResultEntity(List<PfadEntity> pfad, List<String> fehlernachricht, String bildpfad, HttpStatus statusCode)
    {
        this.pfad = pfad;
        this.fehlernachricht = fehlernachricht;
        this.bildpfad = bildpfad;
        this.statusCode = statusCode;
    }

    public List<PfadEntity> getPfad()
    {
        return pfad;
    }

    public List<String> getFehlernachricht()
    {
        return fehlernachricht;
    }

    public HttpStatus getStatusCode()
    {
        return statusCode;
    }

    public String getBildpfad()
    {
        return bildpfad;
    }
}
