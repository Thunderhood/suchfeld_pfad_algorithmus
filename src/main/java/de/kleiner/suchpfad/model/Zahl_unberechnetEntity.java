package de.kleiner.suchpfad.model;

import javax.persistence.*;

@Entity
@Table(name = "unberechnet")
public
class Zahl_unberechnetEntity
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id = 1L;

    private int rechenoperation;
    private Long orginal_id;
    private Long rho;
    private Long wert;
    private Long zeilenindex;
    private Long spaltenindex;
    private Long vorgaengerId;
    private Long rechte_ZahlId;
    private Long untere_ZahlId;
    private Long linke_ZahlId;
    private Long obere_ZahlId;

    Zahl_unberechnetEntity()
    {
    }

    public Long getId()
    {
        return id;
    }

    public Long getOrginal_id()
    {
        return orginal_id;
    }


    public Long getWert()
    {
        return wert;
    }

    public Long getZeilenindex()
    {
        return zeilenindex;
    }

    public Long getSpaltenindex()
    {
        return spaltenindex;
    }

    public Long getVorgaengerId()
    {
        return vorgaengerId;
    }

    public Long getRechte_ZahlId()
    {
        return rechte_ZahlId;
    }

    public Long getUntere_ZahlId()
    {
        return untere_ZahlId;
    }

    public Long getLinke_ZahlId()
    {
        return linke_ZahlId;
    }

    public Long getObere_ZahlId()
    {
        return obere_ZahlId;
    }

    public Long getRho()
    {
        return rho;
    }


    public void setOrginal_id(Long orginal_id)
    {
        this.orginal_id = orginal_id;
    }

    public void setZeilenindex(Long zeilenindex)
    {
        this.zeilenindex = zeilenindex;
    }

    public void setSpaltenindex(Long spaltenindex)
    {
        this.spaltenindex = spaltenindex;
    }

    public void setVorgaengerId(Long vorgaengerId)
    {
        this.vorgaengerId = vorgaengerId;
    }

    public void setRechte_ZahlId(Long rechte_ZahlId)
    {
        this.rechte_ZahlId = rechte_ZahlId;
    }

    public void setUntere_ZahlId(Long untere_ZahlId)
    {
        this.untere_ZahlId = untere_ZahlId;
    }

    public void setLinke_ZahlId(Long linke_ZahlId)
    {
        this.linke_ZahlId = linke_ZahlId;
    }

    public void setObere_ZahlId(Long obere_ZahlId)
    {
        this.obere_ZahlId = obere_ZahlId;
    }


    public Zahl_unberechnetEntity(int rechenoperation, Long orginal_id, Long rho, Long wert, Long zeilenindex, Long spaltenindex, Long vorgaengerId, Long rechte_ZahlId, Long untere_ZahlId, Long linke_ZahlId, Long obere_ZahlId)
    {
        this.wert = wert;
        this.orginal_id = orginal_id;
        this.rho = rho;
        this.rechenoperation = rechenoperation;
        this.zeilenindex = zeilenindex;
        this.spaltenindex = spaltenindex;
        this.vorgaengerId = vorgaengerId;
        this.rechte_ZahlId = rechte_ZahlId;
        this.untere_ZahlId = untere_ZahlId;
        this.linke_ZahlId = linke_ZahlId;
        this.obere_ZahlId = obere_ZahlId;
    }


    public Zahl_unberechnetEntity(Long id, Long orginal_id, int rechenoperation, Long rho, Long wert, Long zeilenindex, Long spaltenindex, Long vorgaengerId, Long rechte_ZahlId, Long untere_ZahlId, Long linke_ZahlId, Long obere_ZahlId)
    {
        this.id = id;
        this.orginal_id = orginal_id;
        this.rechenoperation = rechenoperation;
        this.rho = rho;
        this.wert = wert;
        this.zeilenindex = zeilenindex;
        this.spaltenindex = spaltenindex;
        this.vorgaengerId = vorgaengerId;
        this.rechte_ZahlId = rechte_ZahlId;
        this.untere_ZahlId = untere_ZahlId;
        this.linke_ZahlId = linke_ZahlId;
        this.obere_ZahlId = obere_ZahlId;
    }

    public int getRechenoperation()
    {
        return rechenoperation;
    }

    public void setRechenoperation(int rechenoperation)
    {
        this.rechenoperation = rechenoperation;
    }
}
