package de.kleiner.suchpfad.model;

import javax.persistence.*;

@Entity
@Table(name = "Zahl")
public
class ZahlEntity
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    Long rho;
    Long wert;
    Long zeilenindex;
    Long spaltenindex;
    Long vorgaengerId;
    Long rechte_ZahlId;
    Long untere_ZahlId;
    Long linke_ZahlId;
    Long obere_ZahlId;

    ZahlEntity()
    {
    }

    public Long getId()
    {
        return id;
    }

    public Long getWert()
    {
        return wert;
    }

    public Long getZeilenindex()
    {
        return zeilenindex;
    }

    public Long getSpaltenindex()
    {
        return spaltenindex;
    }

    public Long getVorgaengerId()
    {
        return vorgaengerId;
    }

    public Long getRechte_ZahlId()
    {
        return rechte_ZahlId;
    }

    public Long getUntere_ZahlId()
    {
        return untere_ZahlId;
    }

    public Long getLinke_ZahlId()
    {
        return linke_ZahlId;
    }

    public Long getObere_ZahlId()
    {
        return obere_ZahlId;
    }

    public Long getRho()
    {
        return rho;
    }

    public void setZeilenindex(Long zeilenindex)
    {
        this.zeilenindex = zeilenindex;
    }

    public void setSpaltenindex(Long spaltenindex)
    {
        this.spaltenindex = spaltenindex;
    }

    public void setVorgaengerId(Long vorgaengerId)
    {
        this.vorgaengerId = vorgaengerId;
    }

    public void setRechte_ZahlId(Long rechte_ZahlId)
    {
        this.rechte_ZahlId = rechte_ZahlId;
    }

    public void setUntere_ZahlId(Long untere_ZahlId)
    {
        this.untere_ZahlId = untere_ZahlId;
    }

    public void setLinke_ZahlId(Long linke_ZahlId)
    {
        this.linke_ZahlId = linke_ZahlId;
    }

    public void setObere_ZahlId(Long obere_ZahlId)
    {
        this.obere_ZahlId = obere_ZahlId;
    }

    public ZahlEntity(Long rho, Long wert, Long zeilenindex, Long spaltenindex, Long vorgaengerId, Long rechte_ZahlId, Long untere_ZahlId, Long linke_ZahlId, Long obere_ZahlId)
    {
        this.wert = wert;
        this.rho = rho;
        this.zeilenindex = zeilenindex;
        this.spaltenindex = spaltenindex;
        this.vorgaengerId = vorgaengerId;
        this.rechte_ZahlId = rechte_ZahlId;
        this.untere_ZahlId = untere_ZahlId;
        this.linke_ZahlId = linke_ZahlId;
        this.obere_ZahlId = obere_ZahlId;
    }


    public ZahlEntity(Long id, Long rho, Long wert, Long zeilenindex, Long spaltenindex, Long vorgaengerId, Long rechte_ZahlId, Long untere_ZahlId, Long linke_ZahlId, Long obere_ZahlId)
    {
        this.id = id;
        this.rho = rho;
        this.wert = wert;
        this.zeilenindex = zeilenindex;
        this.spaltenindex = spaltenindex;
        this.vorgaengerId = vorgaengerId;
        this.rechte_ZahlId = rechte_ZahlId;
        this.untere_ZahlId = untere_ZahlId;
        this.linke_ZahlId = linke_ZahlId;
        this.obere_ZahlId = obere_ZahlId;
    }

}
