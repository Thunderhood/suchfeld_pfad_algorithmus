package de.kleiner.suchpfad.repository;

import de.kleiner.suchpfad.model.Zahl_unberechnetEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface UnberechnetRepository extends JpaRepository<Zahl_unberechnetEntity, Long>
{
    List<Zahl_unberechnetEntity> findByZeilenindex(Long zeilenindex);

    List<Zahl_unberechnetEntity> findBySpaltenindex(Long spaltenindex);

    List<Zahl_unberechnetEntity> findByWert(Long wert);

    Optional<Zahl_unberechnetEntity> findById(Long Id);

    List<Zahl_unberechnetEntity> findByRho(Long rho);

    List<Zahl_unberechnetEntity> findByRechenoperation(int rechenoperation);

}
