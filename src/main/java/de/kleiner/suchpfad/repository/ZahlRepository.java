package de.kleiner.suchpfad.repository;

import de.kleiner.suchpfad.model.ZahlEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


@Repository
public interface ZahlRepository extends JpaRepository<ZahlEntity, Long>
{
    List<ZahlEntity> findByZeilenindex(Long zeilenindex);

    List<ZahlEntity> findBySpaltenindex(Long spaltenindex);

    List<ZahlEntity> findByWert(Long wert);

    Optional<ZahlEntity> findById(Long Id);

    List<ZahlEntity> findByRho(Long rho);
}


