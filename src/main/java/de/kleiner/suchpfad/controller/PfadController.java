package de.kleiner.suchpfad.controller;

import de.kleiner.suchpfad.service.PfadfinderService;
import de.kleiner.suchpfad.model.PfadEntity;
import de.kleiner.suchpfad.model.ZahlEntity;
import de.kleiner.suchpfad.service.ZahlenService;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StreamUtils;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@RestController
public class PfadController
{

    private final ZahlenService zahlenService;
    private  final PfadfinderService pfadfinderService;

    

    public PfadController(ZahlenService zahlService, PfadfinderService pfadfinderService)
    {
        this.zahlenService = zahlService;
        this.pfadfinderService = pfadfinderService;
    }


    // ______________________________ GET _____________________________________

    @GetMapping("/zahlen/rho/{rho}")
    public List<ZahlEntity> getZahlenByRho
            (
                    @PathVariable String rho
            )
    {
        List<ZahlEntity> zahlen = zahlenService.suche_alle_Zahlen_mit_selben_rho_heraus(Long.parseLong(rho));
        return zahlen.size() > 0 ? zahlen : null;
    }

    @GetMapping({"/pfad/{gesuchte_zahlen}", "/pfad/{gesuchte_zahlen}/ohne/{ohne_zahlen}"})
    public List<PfadEntity> getPfadfuerZahlByRho
            (
                    @PathVariable String gesuchte_zahlen,
                    @PathVariable(required = false) String ohne_zahlen
            )
    {
        String[] gesucht = gesuchte_zahlen.split(" ");
        String[] ohne = new String[0];
        if (ohne_zahlen != null)
        {
            ohne = ohne_zahlen.split(" ");
        }


        List<PfadEntity> Liste_Pfade = new ArrayList<>();
        for (String naechste_zahl : gesucht)
        {
            List<ZahlEntity> pfad_zur_Zahl = pfadfinderService.baue_pfad_zur_Zahl(naechste_zahl, ohne);
            Liste_Pfade.add(new PfadEntity(pfad_zur_Zahl, naechste_zahl));
        }

        return Liste_Pfade;

    }


    // ________________________________DELETE________________________________________

    @DeleteMapping("/zahlen/clean/rho/{rho}")
    public void loescheAlleZahlenByRho
            (
                    @PathVariable String rho
            )
    {

        zahlenService.loesche_alle_Zahlen_aus_der_Datenbank(Long.parseLong(rho));

    }



}
