package de.kleiner.suchpfad.service;

import de.kleiner.suchpfad.model.ZahlEntity;
import de.kleiner.suchpfad.model.Zahl_unberechnetEntity;
import de.kleiner.suchpfad.repository.UnberechnetRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
@Service
public class PfadfinderService
{
    private long rho;
    private final ZahlenService zahlenService;
    private final UnberechnetRepository unberechnetRepository;
    private final RecheneinheitService recheneinheitService;
    @Autowired
    public PfadfinderService(ZahlenService zahlenService, UnberechnetRepository unberechnetRepository, RecheneinheitService recheneinheitService)
    {
        this.zahlenService = zahlenService;
        this.unberechnetRepository = unberechnetRepository;
        this.recheneinheitService = recheneinheitService;
    }


    public List<ZahlEntity> baue_pfad_zur_Zahl(String naechste_zahl, String[] ohne)
    {

        zahlenService.ermittle_gesuchte_und_verbotene_Zahlen(naechste_zahl, ohne);
        this.rho = zahlenService.getVerbotene_Zahlen().size();

        //Durchlauf des Suchfeldes zu den gesuchten Zahlen
        fuelle_Datenbank_mit_allen_durchlaufenen_Zahlen();

        //Aufbauen der Pfade (kuerzester wird behalten)
        List<ZahlEntity> ListeMitMoeglichenPfadAnfang = zahlenService.suche_alle_Zahlen_mit_selben_rho_heraus(rho)
                .stream().filter(zahlEntity -> zahlEntity.getWert() == Long.parseLong(naechste_zahl)).toList();

        List<ZahlEntity> kuerzesterPfad = new ArrayList<>();
        List<ZahlEntity> aktuellerPfad = new ArrayList<>();

        for (ZahlEntity pfadStart : ListeMitMoeglichenPfadAnfang)
        {
            aktuellerPfad.add(pfadStart);
            ZahlEntity aktuelleZahl = aktuellerPfad.get(0);
            Optional<ZahlEntity> vorgaenger;
            do
            {
                vorgaenger = zahlenService.suche_Zahl_mit_id_aus_Datenbank(aktuelleZahl.getVorgaengerId());
                if (vorgaenger.isPresent())
                {
                    if (vorgaenger.get().getWert() != -1)
                    {
                        aktuellerPfad.add(vorgaenger.get());
                    }
                    aktuelleZahl = vorgaenger.get();
                }
            }
            while (aktuelleZahl.getWert() != -1);

            if (kuerzesterPfad.size() == 0 || aktuellerPfad.size() < kuerzesterPfad.size())
            {
                kuerzesterPfad = aktuellerPfad;
            }
            aktuellerPfad = new ArrayList<>();
        }
        Collections.reverse(kuerzesterPfad);
        return kuerzesterPfad;
    }


    private void fuelle_Datenbank_mit_allen_durchlaufenen_Zahlen()
    {

        //Initialwerte fuer die Datenbank fuer den jeweiligen Rho-Wert (Rho-Wert entspricht die ersten n-Primzahlen ab 3)
        Optional<ZahlEntity> init_Zahl_db = zahlenService.suche_Zahl_in_Datenbank(-1L, rho, -1L, -1L);

        if (init_Zahl_db.isEmpty())
        {
            init_Zahl_db = Optional.of(new ZahlEntity(rho, -1L, -1L, -1L, -1L, -1L, -1L, -1L, -1L));
            zahlenService.speichere_Zahl_in_Datenbank(init_Zahl_db.get());
        }

        Optional<ZahlEntity> zahl_db = zahlenService.suche_Zahl_in_Datenbank(1L, rho, 1L, 1L);
        if (zahl_db.isEmpty())
        {
            zahl_db = Optional.of(new ZahlEntity(rho, 1L, 1L, 1L, init_Zahl_db.get().getId(), -1L, -1L, -1L, -1L));
            zahlenService.speichere_Zahl_in_Datenbank(zahl_db.get());
        }

        //Startwert für die Suchfeldsuche erstellen
        Long[] zahl_db_info = zahlenService.sammle_infos_von_zahl(zahl_db);
        if (unberechnetRepository.findByRho(rho).size() == 0)
        {
            unberechnetRepository.save(new Zahl_unberechnetEntity(1, zahl_db_info[0], zahl_db_info[1], zahl_db_info[2], zahl_db_info[3], zahl_db_info[4], zahl_db_info[5], zahl_db_info[6], zahl_db_info[7], zahl_db_info[8], zahl_db_info[9]));
        }


        //Startpunkt des Suchfeld-Pfad-Algorithmus
        if (recheneinheitService.rechne_Zahl_aus(rho) != 1)
        {
            System.err.println("Kein Pfad gefunden");
        }
    }


}
