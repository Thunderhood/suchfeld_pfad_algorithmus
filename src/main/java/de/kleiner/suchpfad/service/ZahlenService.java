package de.kleiner.suchpfad.service;

import de.kleiner.suchpfad.model.ZahlEntity;
import de.kleiner.suchpfad.model.Zahl_unberechnetEntity;
import de.kleiner.suchpfad.repository.UnberechnetRepository;
import de.kleiner.suchpfad.repository.ZahlRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ZahlenService
{

    private final ArrayList<Long> gesuchte_Zahlen = new ArrayList<>();
    private final ArrayList<Long> gesuchte_Zahlen_2 = new ArrayList<>();



    private final ArrayList<Long> verbotene_Zahlen = new ArrayList<>();

    private final ZahlRepository zahlRepository;
    private final UnberechnetRepository unberechnetRepository;

    @Autowired
    public ZahlenService(ZahlRepository zahlRepository, UnberechnetRepository unberechnetRepository)
    {
        this.zahlRepository = zahlRepository;
        this.unberechnetRepository = unberechnetRepository;
    }

    public ArrayList<Long> getGesuchte_Zahlen()
    {
        return gesuchte_Zahlen;
    }

    public ArrayList<Long> getVerbotene_Zahlen()
    {
        return verbotene_Zahlen;
    }

    public void ermittle_gesuchte_und_verbotene_Zahlen(String gesucht, String[] ohne)
    {
        this.gesuchte_Zahlen.clear();
        this.gesuchte_Zahlen_2.clear();
        this.verbotene_Zahlen.clear();


        this.gesuchte_Zahlen.add(Long.parseLong(gesucht));
        this.gesuchte_Zahlen_2.add(Long.parseLong(gesucht));


        for (String zahl : ohne)
        {
            this.verbotene_Zahlen.add(Long.parseLong(zahl));
        }

        //lösche Zahlen aus gesucht, die auch in verboten drinne sind
        for (Long zahlenwert : this.verbotene_Zahlen)
        {
            for (int i = 0; i < this.gesuchte_Zahlen.size(); )
            {
                if (this.gesuchte_Zahlen.get(i).equals(zahlenwert))
                {
                    this.gesuchte_Zahlen_2.remove(this.gesuchte_Zahlen.get(i));
                    this.gesuchte_Zahlen.remove(this.gesuchte_Zahlen.get(i));
                }
                else
                {
                    i++;
                }
            }
        }
    }

    public Optional<ZahlEntity> suche_Zahl_in_Datenbank(Long wert, Long rho, Long zeile, Long spalte)
    {
        List<ZahlEntity> liste_von_Zahlen_aus_Datenbank = suche_alle_Zahlen_mit_selben_rho_heraus(rho);

        return liste_von_Zahlen_aus_Datenbank.stream()
                .filter(zahl -> zahl.getWert().equals(wert))
                .filter(zahl -> zahl.getZeilenindex().equals(zeile))
                .filter(zahl -> zahl.getSpaltenindex().equals(spalte))
                .findFirst();

    }

    public Long[] sammle_infos_von_zahl(Optional<ZahlEntity> zahl_db)
    {
        //Datenbankinformationen:  id, rho, wert, zeilenindex, spaltenindex, vorgaengerId, rechteZahlId, untereZahlId, linkeZahlid, obereZahlId
        ZahlEntity zahl = zahl_db.get();
        return new Long[]{zahl.getId(), zahl.getRho(), zahl.getWert(), zahl.getZeilenindex(), zahl.getSpaltenindex(), zahl.getVorgaengerId(), zahl.getRechte_ZahlId(), zahl.getUntere_ZahlId(), zahl.getLinke_ZahlId(), zahl.getObere_ZahlId()};
    }


    public List<ZahlEntity> suche_alle_Zahlen_mit_selben_rho_heraus(Long rho)
    {
        return zahlRepository.findByRho(rho);
    }

    public List<Zahl_unberechnetEntity> suche_alle_unberechneten_Zahlen_mit_selben_rho_heraus(Long rho)
    {
        return unberechnetRepository.findByRho(rho);
    }

    public List<Zahl_unberechnetEntity> suche_unberechnete_Zahlen_mithilfe_rho(Long rho)
    {
        return unberechnetRepository.findByRho(rho);
    }


    public void loesche_alle_Zahlen_aus_der_Datenbank(Long rho)
    {
        zahlRepository.deleteAll(zahlRepository.findByRho(rho));
        unberechnetRepository.deleteAll(unberechnetRepository.findByRho(rho));
    }

    public void loesche_unberechnete_Zahl_aus_Datenbank(Long id)
    {
        unberechnetRepository.deleteById(id);
    }

    public Optional<ZahlEntity> suche_Zahl_mit_id_aus_Datenbank(Long id)
    {
        return zahlRepository.findById(id);
    }

    public void speichere_Zahl_in_Datenbank(ZahlEntity zahl)
    {
        zahlRepository.save(zahl);
    }

    public void speichere_unberechnete_Zahl_in_Datenbank(Zahl_unberechnetEntity zahl_unberechnetEntity)
    {
        unberechnetRepository.save(zahl_unberechnetEntity);
    }
}
