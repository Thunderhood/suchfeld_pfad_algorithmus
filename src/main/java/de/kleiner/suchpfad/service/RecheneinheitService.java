package de.kleiner.suchpfad.service;

import de.kleiner.suchpfad.model.ZahlEntity;
import de.kleiner.suchpfad.model.Zahl_unberechnetEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
@Service
public class RecheneinheitService
{

    private final ZahlenService zahlenService;

    public RecheneinheitService(ZahlenService zahlenService)
    {
        this.zahlenService = zahlenService;
    }

    protected int teste_Zahl(long Zahl)
    {
        for (Long verbotene_Zahl : zahlenService.getVerbotene_Zahlen())
        {
            if (verbotene_Zahl == Zahl) return -1;
        }
        for (Long gesuchte_Zahl : zahlenService.getGesuchte_Zahlen())
        {
            if (gesuchte_Zahl == Zahl)
            {
                zahlenService.getGesuchte_Zahlen().remove(Zahl);
                break;
            }
        }
        return 0;
    }

    public long addiere_Zahl(long Zahl)
    {
        Zahl++;
        int ergebnis_test = teste_Zahl(Zahl);
        if (ergebnis_test == 0)
        {
            return Zahl;
        }
        else
        {
            return ergebnis_test;
        }
    }

    public long subtrahiere_Zahl(long Zahl)
    {
        Zahl--;
        int ergebnis_test = teste_Zahl(Zahl);
        if (ergebnis_test == 0)
        {
            return Zahl;
        }
        else if (ergebnis_test == -1)
        {
            return -1;
        }
        else
        {
            return -2;
        }
    }

    public long multipliziere_Zahl(long Zahl, long Faktor)
    {
        Zahl = Zahl * Faktor;
        int ergebnis_test = teste_Zahl(Zahl);
        if (ergebnis_test == -1)
        {
            return -1;
        }
        else
        {
            return Zahl;
        }

    }

    public long dividiere_Zahl(long Zahl, Long Divisor)
    {
        if (Divisor == 0)
        {
            return -1;
        }
        else if (Zahl % Divisor == 0)
        {
            Zahl = Zahl / Divisor;
        }
        else
        {
            return -1;
        }
        int ergebnis_test = teste_Zahl(Zahl);
        if (ergebnis_test == -1)
        {
            return -1;
        }
        else
        {
            return Zahl;
        }
    }

    public int rechne_Zahl_aus(Long rho)
    {
        List<ZahlEntity> zusuchende_Zahlen = zahlenService.suche_alle_Zahlen_mit_selben_rho_heraus(rho);
        List<Long> loesche_gesuchte_zahlen = new ArrayList<>();

        for (Long gesuchte_zahl : zahlenService.getGesuchte_Zahlen())
        {
            if (zusuchende_Zahlen.stream().anyMatch(zahl_berechnet -> zahl_berechnet.getWert().equals(gesuchte_zahl)))
            {
                loesche_gesuchte_zahlen.add(gesuchte_zahl);
            }
        }
        zahlenService.getGesuchte_Zahlen().removeAll(loesche_gesuchte_zahlen);

        List<Zahl_unberechnetEntity> unbearbeitete_Zahlen = zahlenService.suche_alle_unberechneten_Zahlen_mit_selben_rho_heraus(rho);
        int finish;
        while (unbearbeitete_Zahlen.size() > 0)
        {
            if (zahlenService.getGesuchte_Zahlen().isEmpty()) return 1;
            ausgabe_noch_fehlende_gesuchte_zahlen();

            List<Zahl_unberechnetEntity> Nicht_Subtraktion = unbearbeitete_Zahlen.stream().filter(zahl_unberechnetEntity -> zahl_unberechnetEntity.getRechenoperation() == 1).toList();
            for (Zahl_unberechnetEntity zahl_ohne_sub : Nicht_Subtraktion)
            {
                finish = errechne_Nachfolgerzahlen_und_packe_diese_in_die_Datenbank(rho, 1, zahl_ohne_sub);
                zahlenService.loesche_unberechnete_Zahl_aus_Datenbank(zahl_ohne_sub.getId());
                if (finish == 1) break;
            }

            if (zahlenService.getGesuchte_Zahlen().isEmpty()) return 1;
            ausgabe_noch_fehlende_gesuchte_zahlen();

            List<Zahl_unberechnetEntity> Nicht_Division = unbearbeitete_Zahlen.stream().filter(zahl_unberechnetEntity -> zahl_unberechnetEntity.getRechenoperation() == 2).toList();
            for (Zahl_unberechnetEntity zahl_ohne_div : Nicht_Division)
            {
                finish = errechne_Nachfolgerzahlen_und_packe_diese_in_die_Datenbank(rho, 2, zahl_ohne_div);
                zahlenService.loesche_unberechnete_Zahl_aus_Datenbank(zahl_ohne_div.getId());
                if (finish == 1) break;
            }

            if (zahlenService.getGesuchte_Zahlen().isEmpty()) return 1;
            ausgabe_noch_fehlende_gesuchte_zahlen();

            List<Zahl_unberechnetEntity> Nicht_Addition = unbearbeitete_Zahlen.stream().filter(zahl_unberechnetEntity -> zahl_unberechnetEntity.getRechenoperation() == -1).toList();
            for (Zahl_unberechnetEntity zahl_ohne_add : Nicht_Addition)
            {
                finish = errechne_Nachfolgerzahlen_und_packe_diese_in_die_Datenbank(rho, -1, zahl_ohne_add);
                zahlenService.loesche_unberechnete_Zahl_aus_Datenbank(zahl_ohne_add.getId());
                if (finish == 1) break;
            }

            if (zahlenService.getGesuchte_Zahlen().isEmpty()) return 1;
            ausgabe_noch_fehlende_gesuchte_zahlen();

            List<Zahl_unberechnetEntity> Nicht_Multiplikation = unbearbeitete_Zahlen.stream().filter(zahl_unberechnetEntity -> zahl_unberechnetEntity.getRechenoperation() == -2).toList();
            for (Zahl_unberechnetEntity zahl_ohne_mul : Nicht_Multiplikation)
            {

                finish = errechne_Nachfolgerzahlen_und_packe_diese_in_die_Datenbank(rho, -2, zahl_ohne_mul);
                zahlenService.loesche_unberechnete_Zahl_aus_Datenbank(zahl_ohne_mul.getId());
                if (finish == 1) break;
            }
            unbearbeitete_Zahlen = zahlenService.suche_unberechnete_Zahlen_mithilfe_rho(rho);

        }

        return 0;
    }


    protected int errechne_Nachfolgerzahlen_und_packe_diese_in_die_Datenbank(Long rho, int modus, Zahl_unberechnetEntity zahl)
    {
        //modus: -1-> ohne Plus 1-> ohne Minus -2-> ohne Durch 2 -> ohne Mal
        if (modus != -1)
        {
            //Pfad Addition
            long add_Zahl = addiere_Zahl(zahl.getWert());
            if (add_Zahl > 0)
            {

                Optional<ZahlEntity> neue_Zahl = zahlenService.suche_Zahl_in_Datenbank(add_Zahl, rho, zahl.getZeilenindex(), zahl.getSpaltenindex() + 1);
                if (neue_Zahl.isEmpty())
                {
                    neue_Zahl = Optional.of(new ZahlEntity(rho, add_Zahl, zahl.getZeilenindex(), zahl.getSpaltenindex() + 1, zahl.getOrginal_id(), -1L, -1L, -1L, -1L));
                }
                neue_Zahl.get().setLinke_ZahlId(zahl.getOrginal_id());
                zahlenService.speichere_Zahl_in_Datenbank(neue_Zahl.get());
                Optional<ZahlEntity> aktuelle_Zahl = zahlenService.suche_Zahl_mit_id_aus_Datenbank(zahl.getOrginal_id());

                aktuelle_Zahl.get().setRechte_ZahlId(zahlenService.suche_Zahl_in_Datenbank(add_Zahl, rho, zahl.getZeilenindex(), zahl.getSpaltenindex() + 1).get().getId());
                zahlenService.speichere_Zahl_in_Datenbank(aktuelle_Zahl.get());
                Long[] zahl_db_info = zahlenService.sammle_infos_von_zahl(neue_Zahl);
                zahlenService.speichere_unberechnete_Zahl_in_Datenbank(new Zahl_unberechnetEntity(1, zahl_db_info[0], zahl_db_info[1], zahl_db_info[2], zahl_db_info[3], zahl_db_info[4], zahl_db_info[5], zahl_db_info[6], zahl_db_info[7], zahl_db_info[8], zahl_db_info[9]));
            }
        }

        if (modus != 1)
        {
            //Pfad Subtraktion
            long sub_Zahl = subtrahiere_Zahl(zahl.getWert());
            if (zahl.getSpaltenindex() - 1 < 1) sub_Zahl = -1;
            if (sub_Zahl > 0)
            {

                Optional<ZahlEntity> neue_Zahl = zahlenService.suche_Zahl_in_Datenbank(sub_Zahl, rho, zahl.getZeilenindex(), zahl.getSpaltenindex() - 1);
                if (neue_Zahl.isEmpty())
                {
                    neue_Zahl = Optional.of(new ZahlEntity(rho, sub_Zahl, zahl.getZeilenindex(), zahl.getSpaltenindex() - 1, zahl.getOrginal_id(), -1L, -1L, -1L, -1L));
                }
                neue_Zahl.get().setRechte_ZahlId(zahl.getOrginal_id());
                zahlenService.speichere_Zahl_in_Datenbank(neue_Zahl.get());
                Optional<ZahlEntity> aktuelle_Zahl = zahlenService.suche_Zahl_mit_id_aus_Datenbank(zahl.getOrginal_id());
                aktuelle_Zahl.get().setLinke_ZahlId(neue_Zahl.get().getId());
                zahlenService.speichere_Zahl_in_Datenbank(aktuelle_Zahl.get());
                Long[] zahl_db_info = zahlenService.sammle_infos_von_zahl(neue_Zahl);
                zahlenService.speichere_unberechnete_Zahl_in_Datenbank(new Zahl_unberechnetEntity(-1, zahl_db_info[0], zahl_db_info[1], zahl_db_info[2], zahl_db_info[3], zahl_db_info[4], zahl_db_info[5], zahl_db_info[6], zahl_db_info[7], zahl_db_info[8], zahl_db_info[9]));
            }
        }

        if (modus != -2)
        {
            //Pfad Multiplkation
            long mul_Zahl = multipliziere_Zahl(zahl.getWert(), zahl.getZeilenindex() + 1);
            if (mul_Zahl > 0)
            {

                Optional<ZahlEntity> neue_Zahl = zahlenService.suche_Zahl_in_Datenbank(mul_Zahl, rho, zahl.getZeilenindex() + 1, zahl.getSpaltenindex());
                if (neue_Zahl.isEmpty())
                {
                    neue_Zahl = Optional.of(new ZahlEntity(rho, mul_Zahl, zahl.getZeilenindex() + 1, zahl.getSpaltenindex(), zahl.getOrginal_id(), -1L, -1L, -1L, -1L));
                }
                neue_Zahl.get().setObere_ZahlId(zahl.getOrginal_id());
                zahlenService.speichere_Zahl_in_Datenbank(neue_Zahl.get());
                Optional<ZahlEntity> aktuelle_Zahl = zahlenService.suche_Zahl_mit_id_aus_Datenbank(zahl.getOrginal_id());
                aktuelle_Zahl.get().setUntere_ZahlId(neue_Zahl.get().getId());
                zahlenService.speichere_Zahl_in_Datenbank(aktuelle_Zahl.get());
                Long[] zahl_db_info = zahlenService.sammle_infos_von_zahl(neue_Zahl);
                zahlenService.speichere_unberechnete_Zahl_in_Datenbank(new Zahl_unberechnetEntity(2, zahl_db_info[0], zahl_db_info[1], zahl_db_info[2], zahl_db_info[3], zahl_db_info[4], zahl_db_info[5], zahl_db_info[6], zahl_db_info[7], zahl_db_info[8], zahl_db_info[9]));
            }
        }

        if (modus != 2)
        {
            //Pfad Division
            long div_Zahl = dividiere_Zahl(zahl.getWert(), zahl.getZeilenindex());
            if (zahl.getZeilenindex() - 1 < 1) div_Zahl = -1;
            if (div_Zahl > 0)
            {

                Optional<ZahlEntity> neue_Zahl = zahlenService.suche_Zahl_in_Datenbank(div_Zahl, rho, zahl.getZeilenindex() - 1, zahl.getSpaltenindex());
                if (neue_Zahl.isEmpty())
                {
                    neue_Zahl = Optional.of(new ZahlEntity(rho, div_Zahl, zahl.getZeilenindex() - 1, zahl.getSpaltenindex(), zahl.getOrginal_id(), -1L, -1L, -1L, -1L));
                }
                neue_Zahl.get().setUntere_ZahlId(zahl.getOrginal_id());
                zahlenService.speichere_Zahl_in_Datenbank(neue_Zahl.get());
                Optional<ZahlEntity> aktuelle_Zahl = zahlenService.suche_Zahl_mit_id_aus_Datenbank(zahl.getOrginal_id());
                aktuelle_Zahl.get().setObere_ZahlId(neue_Zahl.get().getId());
                zahlenService.speichere_Zahl_in_Datenbank(aktuelle_Zahl.get());
                Long[] zahl_db_info = zahlenService.sammle_infos_von_zahl(neue_Zahl);
                zahlenService.speichere_unberechnete_Zahl_in_Datenbank(new Zahl_unberechnetEntity(-2, zahl_db_info[0], zahl_db_info[1], zahl_db_info[2], zahl_db_info[3], zahl_db_info[4], zahl_db_info[5], zahl_db_info[6], zahl_db_info[7], zahl_db_info[8], zahl_db_info[9]));


            }

            if (zahlenService.getGesuchte_Zahlen().isEmpty()) return 1;
        }
        return 0;
    }

    private  void ausgabe_noch_fehlende_gesuchte_zahlen()
    {
        StringBuilder gesucht = new StringBuilder("gesuchte Zahlen:");
        for (Long z : zahlenService.getGesuchte_Zahlen())
        {
            gesucht.append(" ").append(z);
        }
        System.out.println(gesucht);
    }
}